

document.addEventListener("DOMContentLoaded", () => {
  document.getElementById("nav-mode_switch").addEventListener("click", () => {
    document.body.classList.toggle("dark");

    localStorage.setItem(
      "theme",
      document.body.classList.contains("dark") ? "dark" : "light"
    );
  });

  if (localStorage.getItem("theme") === "dark") {
    document.body.classList.add("dark");
  }
});

const loading = document.getElementById("loading");
setTimeout(() => {
  loading.classList.add("loading-none");
}, 4000);


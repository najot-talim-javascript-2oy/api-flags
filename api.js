let usersRow = document.querySelector(".users-row");
const search = document.querySelector("#search");
const dropDown = document.querySelector(".dropdownMenu");
const dropdownOptions = document.querySelector(".drop-options");
const regions = document.querySelectorAll(".regions");
const regionName = document.getElementsByClassName("regionName");

regions.forEach((region) => {
  region.addEventListener("click", (e) => {
    Array.from(regionName).forEach((element) => {
      if (
        element.innerText.includes(region.innerText) ||
        region.innerText === "All"
      ) {
        element.parentElement.parentElement.style.display = "grid";
      } else {
        element.parentElement.parentElement.style.display = "none";
      }
    });
  });
});

dropDown.addEventListener('click', (e) =>{
  dropdownOptions.classList.toggle("show-options")
})

async function getData() {
  let res = await fetch(`https://restcountries.com/v3.1/all`)
    .then((response) => response.json())
    .then((json) => json);
  usersRow.innerHTML = "";
  res.forEach((country) => {
    usersRow.innerHTML += renderUsers(country);
  });
}
getData();


function renderUsers(data) {
  return ` 
  <div class="Cards">
      <div class="card" data-tilt>
        <img src="${data.flags.png}" alt="">
        <h2 class="countryName">Name: <span class="Ism">${
          data.name.common
        }</span></h2>
        <h2>Population: <span class="Ism">
          ${
            data.population > 10 ** 6
              ? Math.floor(data.population / 10 ** 6) + " mln"
              : Math.floor(data.population / 1000) + " K"
          }
        </span></h2>
        <h2 class="regionName">Region: <span class="Ism">${
          data.region
        }</span></h2>

        <div class="bt">
            <div class="profile-details">
                <a href="#" onclick="saveId(${data.name.common})" class="button">Full Details</a>
            </div>
        </div>
        
        </div>
        </div>
  `;
}


// country.addEventListener("click", () =>{
//   showCountryDetail()
// })

const countryName = document.getElementsByClassName("countryName");

search.addEventListener("input", e => {
  Array.from(countryName).forEach(country => {
      if (
        country.innerText.toLowerCase().includes(search.value.toLowerCase())
      ) {
        country.parentElement.parentElement.style.display = "grid";
      } else {
        country.parentElement.parentElement.style.display = "none";
      }
  });
});

// const back = document.querySelector(".back")
// const countryModal = document.querySelector(".back");
// back.addEventListener(("click"), ()=>{
//   countryModal.classList.toggle("show")
// })

